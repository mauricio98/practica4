<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of alumno
 *
 * @author GiselaE
 */
 
include_once("database.php");

class Alumno{
    
    private $pdo;    
    public $codAlumno;
    public $nombreAlumno;
    public $apellidoAlumno;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT codAlumno, nombreAlumno, apellidoAlumno FROM alumno");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($codAlumno)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT codAlumno, nombreAlumno, apellidoAlumno FROM alumno WHERE codAlumno = ?");
			          

			$stm->execute(array($codAlumno));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM alumno WHERE codAlumno = ?");			          

			$stm->execute(array($data->codAlumno));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE alumno SET 
						nombreAlumno = ?, 
						apellidoAlumno = ?
				    WHERE codAlumno = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->nombreAlumno, 
                        $data->apellidoAlumno,
                        $data->codAlumno
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Alumno $data)
	{
		try{
		$sql = "INSERT INTO alumno(nombreAlumno,apellidoAlumno) 
		        VALUES (?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombreAlumno, 
                    $data->apellidoAlumno
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}