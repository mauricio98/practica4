<html>
<head>
	<title>Ejemplo de Operaciones de Comprobacion</title>
</head>
<body>
	<h1>Ejemplo de Operaciones Comparaciones en PHP</h1>
	<?php
	$a = 8;
	$b = 3;
	$c = 3;
	echo $a == $b, "<br>";    /* == Es una relación binaria que representa una igualdad. */
	echo $a != $b, "<br>";    /* != El operador de desigualdad devuelve falso si sus operandos son iguales, de lo contrario es verdadero. */
	echo $a < $b, "<br>";     /* < Es un símbolo matemático que denota una desigualdad entre dos valores.*/
	echo $a > $b, "<br>";     /* Típicamente usado en contextos matemáticos para indicar que la cantidad a la izquierda del signo es más grande que la cantidad a la derecha del signo.*/
	echo $a >= $b, "<br>";    /* >= Matemáticas 
Signo matemático que, colocado entre dos cantidades, indica que la de la izquierda tiene mayor o igual valor que la de la derecha.*/
	echo $a <= $b, "<br>";    /* <= Signo matemático que, colocado entre dos cantidades, indica que la de la izquierda tiene menor o igual valor que la de la derecha.*/
	?>
</body>
</html>