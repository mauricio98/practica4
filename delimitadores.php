<!DOCTYPE html>
<html lang="es">
<head>
	<title>Delimitadores de código PHP</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/tabs.css"/>
	<link rel="stylesheet" type="text/css" href="css/delimiters.css"/>
</head>
<body>
<header>
	<h1>Los delimitadores de código PHP</h1><br>
</header>
<section>
	<article>
		<div class="contenedor-tabs">
			<?php
				echo "<span clas=\"diana\" id=\"una\"></span>\n";
			echo "<div class=\"tab\">\n";
			echo "<a href=\"#una\" class=\"tab-e\">Estilo XML</a>\n";
			echo "<div class=\"first\">\n";
			echo "<p class=\"xmltag\">\n";
			echo "Este texto esta escrito en PHP usando etiquetas más ";
			echo "usuales y recomendadas para delimitar el codigo PHP, que son: ";
			echo "&lt;?php ... ?&gt;.<br>\n";
			echo "<p>\n";
			echo "</div>\n";
			echo "</div>\n";
			?>
		</div>
	</article>
</section>

</body>
</html>